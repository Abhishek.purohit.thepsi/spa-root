import { registerApplication, start } from "single-spa";
import {
  constructApplications,
  constructRoutes,
  constructLayoutEngine,
} from "single-spa-layout";
import microfrontendLayout from "./layout.html";

const routes = constructRoutes(microfrontendLayout);
const applications = constructApplications({
  routes,
  loadApp({ name }) {
    return System.import(name);
  },
});
const layoutEngine = constructLayoutEngine({ routes, applications });

applications.forEach(registerApplication);
layoutEngine.activate();
start();

// import { registerApplication, start, LifeCycles } from "single-spa";

// registerApplication({
//   name: "@psi/react-navbar",
//   app: () =>
//     System.import<LifeCycles>(
//       "@psi/react-navbar"
//     ),
//   activeWhen: ["/"],
// });

// // registerApplication({
// //   name: "@psi/navbar",
// //   app: () => System.import("@psi/navbar"),
// //   activeWhen: ["/"]
// // });

// start({
//   urlRerouteOnly: true,
// });
